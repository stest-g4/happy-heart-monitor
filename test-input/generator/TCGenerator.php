<?php
function generateAllTestCases($tcAllFilename = 'TC-All.txt', $withDefaultTC = TRUE) {
	$tcAll = file($tcAllFilename);

	$tcUnwantedCharacters = [
		"“", "”", "\"",
	];

	if ($withDefaultTC) {
		file_put_contents("TC-xx.txt", "");
	}
	for ($idx = 0; $idx < count($tcAll) - 1; $idx += 2) {
		$tcFilename = trim($tcAll[$idx]) . ".txt";
		$tcContent = trim($tcAll[$idx + 1]);
		$tcContent = str_replace($tcUnwantedCharacters, "", $tcContent);
		echo($tcFilename . " : " . $tcContent . "\n");
		file_put_contents($tcFilename, $tcContent);
	}
}

function generatePriorityTestCases($tcPriorityFilename = 'TC-Priority.txt') {
	$tcPriority = file($tcPriorityFilename);

	for ($idx = 0; $idx < count($tcPriority) - 1; $idx += 6) {
		$tcFilename = trim($tcPriority[$idx]) . ".txt";
		$tcReferences = trim($tcPriority[$idx + 5]);
		$tcReferences = explode(",", $tcReferences);
		foreach ($tcReferences as $key => $tcReference) {
			$tcReferenceFilename = trim($tcReference) . ".txt";
			$tcReferences[$key] = file_get_contents($tcReferenceFilename);
		}

		$tcContent = implode(" ", $tcReferences);
		echo($tcFilename . " : " . $tcContent . "\n");
		file_put_contents($tcFilename, $tcContent);
	}
}

function generateAlertMessage($tcAllFilename = 'TC-Message.txt', $withDefaultTC = TRUE) {
	$tcAll = file($tcAllFilename);

	$tcUnwantedCharacters = [
		"“", "”", "\"",
	];

	for ($idx = 0; $idx < count($tcAll) - 1; $idx += 3) {
		$tcFilename = trim($tcAll[$idx]) . ".txt";
		$tcReference = trim($tcAll[$idx + 1]);
		
		$tcContent = file_get_contents($tcReference . ".txt");
		
		$tcContent .= "\n" . trim($tcAll[$idx + 2]);
		$tcContent = str_replace($tcUnwantedCharacters, "", $tcContent);
		echo($tcFilename . " : \n" . $tcContent . "\n");
		file_put_contents($tcFilename, $tcContent);
	}
}

function generateLoadTest($pulse = "", $oxygen = "", $pressure = "", $repeat = 1000, $filename = "TC-Load.txt"){
	$input = [$pulse, $oxygen, $pressure];
	$input = implode(" ", $input) . "\n";
	for(; $repeat > 0; --$repeat){
		file_put_contents($filename, $input, FILE_APPEND);
	}
}

//generateAllTestCases();
//generatePriorityTestCases();
//generateAlertMessage();
generateLoadTest("100", "20");
