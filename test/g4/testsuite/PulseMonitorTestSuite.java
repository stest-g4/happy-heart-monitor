/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.testsuite;

import g4.component.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author simaremare
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	PulseMonitorAcceptedTest.class,
	PulseMonitorRejectedTest.class,
	PulseMonitorWithHighAlarmAcceptedTest.class,
	PulseMonitorWithMediumAlarmAcceptedTest.class,
	PulseMonitorWithLowAlarmAcceptedTest.class,
	PulseMonitorWithImpossibleAlarmAcceptedTest.class,
	PulseMonitorWithNormalAlarmAcceptedTest.class,
	PulseMonitorMessageAcceptedTest.class
})
public class PulseMonitorTestSuite {
}
