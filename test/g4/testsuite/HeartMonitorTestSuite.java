/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.testsuite;

import g4.component.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author simaremare
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	HeartMonitorAcceptedTest.class,
	HeartMonitorRejectedTest.class,
	HeartMonitorWithPulsePriorityAcceptedTest.class,
	HeartMonitorWithOxygenLevelPriorityAcceptedTest.class,
	HeartMonitorWithBloodPressurePriorityAcceptedTest.class,
	HeartMonitorWithNormalAlarmAcceptedTest.class
})
public class HeartMonitorTestSuite {
}
