/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.testsuite;

import g4.component.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author simaremare
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	OxygenLevelMonitorAcceptedTest.class,
	OxygenLevelMonitorRejectedTest.class,
	OxygenLevelMonitorWithHighAlarmAcceptedTest.class,
	OxygenLevelMonitorWithMediumAlarmAcceptedTest.class,
	OxygenLevelMonitorWithLowAlarmAcceptedTest.class,
	OxygenLevelMonitorWithImpossibleAlarmAcceptedTest.class,
	OxygenLevelMonitorWithNormalAlarmAcceptedTest.class,
	OxygenLevelMonitorWithAverageAcceptedTest.class,
	OxygenLevelMonitorMessageAcceptedTest.class
})
public class OxygenLevelMonitorTestSuite {
}
