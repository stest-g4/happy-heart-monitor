/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author simaremare
 */
public class AlertSeverityTest {

	public AlertSeverityTest() {
	}

	/**
	 * Test of process method, of class PulseMonitor.
	 */
	@Test
	public void testProcess() {
		AlertSeverity alertSeverity = AlertSeverity.HIGH;		
		String expected = "HIGH";
		assertEquals(expected, alertSeverity.toString());
		
		alertSeverity = AlertSeverity.MEDIUM;
		expected = "MEDIUM";
		assertEquals(expected, alertSeverity.toString());
		
		alertSeverity = AlertSeverity.LOW;
		expected = "LOW";
		assertEquals(expected, alertSeverity.toString());
		
		alertSeverity = AlertSeverity.IMPOSSIBLE;
		expected = "IMPOSSIBLE";
		assertEquals(expected, alertSeverity.toString());
		
		alertSeverity = AlertSeverity.INVALID_DATA;
		expected = "INVALID_DATA";
		assertEquals(expected, alertSeverity.toString());
		
		alertSeverity = AlertSeverity.NORMAL;
		expected = "NORMAL";
		assertEquals(expected, alertSeverity.toString());
	}

}
