/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.component;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author simaremare
 */
public class AlertTierTest {

	public AlertTierTest() {
	}

	/**
	 * Test of process method, of class PulseMonitor.
	 */
	@Test
	public void testProcess() {
		AlertTier alertTier = AlertTier.UPPER;		
		String expected = "high";
		assertEquals(expected, alertTier.toString());
		
		alertTier = AlertTier.MIDDLE;		
		expected = "";
		assertEquals(expected, alertTier.toString());
		
		alertTier = AlertTier.LOWER;
		expected = "low";
		assertEquals(expected, alertTier.toString());
	}

}
