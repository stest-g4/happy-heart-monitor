/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.component;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author simaremare
 */
public class AlertTypeTest {

	public AlertTypeTest() {
	}

	/**
	 * Test of process method, of class PulseMonitor.
	 */
	@Test
	public void testProcess() {
		AlertType alertType = AlertType.PULSE;		
		String expected = "Pulse";
		assertEquals(expected, alertType.toString());
		
		alertType = AlertType.OXYGEN_LEVEL;		
		expected = "Oxygen level";
		assertEquals(expected, alertType.toString());
		
		alertType = AlertType.BLOOD_PRESSURE;		
		expected = "Blood pressure";
		assertEquals(expected, alertType.toString());
		
		alertType = AlertType.BAD_DATA;		
		expected = "Bad data";
		assertEquals(expected, alertType.toString());
	}

}
