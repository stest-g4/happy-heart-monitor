/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author simaremare
 */
@RunWith(value = Parameterized.class)
public class OxygenLevelMonitorWithAverageAcceptedTest {

	private String testcaseDir = System.getProperty("user.dir") + "\\test-input\\";
	private String testcaseExtension = ".txt";
	private String testcase = null;

	public OxygenLevelMonitorWithAverageAcceptedTest(String _testcase) {
		testcase = _testcase;
	}

	@Parameters(name = "{index}: process({0})")
	public static Iterable<Object[]> data1() {
		return Arrays.asList(new Object[][]{
			{"TC-121"}, {"TC-122"}, {"TC-123"},
			{"TC-124"}, {"TC-125"}, {"TC-126"},
			{"TC-127"}
		});
	}

	/**
	 * Test of process method, of class PulseMonitor.
	 */
	@Test
	public void testProcess() {
		String testcasePath = testcaseDir + testcase + testcaseExtension;
		String testcaseExpPath = testcaseDir + testcase + "-EXP" + testcaseExtension;
		File file = new File(testcasePath);
		File expFile = new File(testcaseExpPath);

		if (file.exists()) {
			OxygenLevelMonitor oxygenLevelMonitor = new OxygenLevelMonitor();
			String[] input = getLines(readFile(file.getPath()));
			String[] expInput = getLines(readFile(expFile.getPath()));

			for (int idx = 0; idx < input.length; ++idx) {
				Alert result = oxygenLevelMonitor.process(input[idx].trim());
				if (result != null) {
					assertEquals(AlertType.OXYGEN_LEVEL, result.getType());
					System.out.println(idx + " " + input[idx].trim() + ": " + "(" + expInput[idx].trim() + ")" + " | " + result.getStrValue());
					if (result.getSeverity() == AlertSeverity.INVALID_DATA) {
						assertNull(result.getStrValue());
						System.out.println("desc: " + result.describe());
					} else {
						assertEquals("(" + expInput[idx].trim() + ")", result.getStrValue());
					}
				}else{
					System.out.println(idx + " " + input[idx].trim() + ": " + "(" + expInput[idx].trim() + ")");
				}
			}

		} else {
			fail("Cannot access testcase file.");
		}
	}

	static String[] getLines(String input) {
		return (input.split("\n"));
	}

	static String readFile(String path) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, Charset.defaultCharset());
		} catch (IOException ioe) {

		}
		return null;
	}

}
