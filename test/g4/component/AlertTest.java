/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author simaremare
 */
public class AlertTest {

	public AlertTest() {
	}

	/**
	 * Test of process method, of class PulseMonitor.
	 */
	@Test
	public void testInitialization() {
		Alert alert = new Alert();
		assertNotNull(alert);
		
		alert = new Alert(AlertType.PULSE, AlertSeverity.MEDIUM);
		assertNotNull(alert);
		assertEquals(AlertType.PULSE, alert.getType());
		
		alert = new Alert(AlertType.PULSE, AlertSeverity.MEDIUM);
		assertNotNull(alert);
		assertEquals(AlertType.PULSE, alert.getType());
		assertEquals(AlertSeverity.MEDIUM, alert.getSeverity());
		
		alert = new Alert(AlertType.PULSE, AlertSeverity.MEDIUM, AlertTier.UPPER, 25);
		assertNotNull(alert);
		assertEquals(AlertType.PULSE, alert.getType());
		assertEquals(AlertSeverity.MEDIUM, alert.getSeverity());
		assertEquals(AlertTier.UPPER, alert.getTier());
		assertEquals(25, alert.getValue());
	}

}
