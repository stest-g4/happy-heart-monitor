/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g4.component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author simaremare
 */
@RunWith(value = Parameterized.class)
public class BloodPressureMonitorRejectedTest {

	private String testcaseDir = System.getProperty("user.dir") + "\\test-input\\";
	private String testcaseExtension = ".txt";
	private String testcase = null;

	public BloodPressureMonitorRejectedTest(String _testcase) {
		testcase = _testcase;
	}

	@Parameters(name = "{index}: process({0})")
	public static Iterable<Object[]> data1() {
		return Arrays.asList(new Object[][]{
			{"TC-23"}, {"TC-25"}, {"TC-26"},
			{"TC-27"}, {"TC-30"}, {"TC-32"},
			{"TC-33"}, {"TC-34"}
		});
	}

	/**
	 * Test of process method, of class PulseMonitor.
	 */
	@Test
	public void testProcess() {
		String testcasePath = testcaseDir + testcase + testcaseExtension;
		File file = new File(testcasePath);

		if (file.exists()) {
			BloodPressureMonitor bloodPressureMonitor = new BloodPressureMonitor();
			String input = readFile(file.getPath());

			Alert result = bloodPressureMonitor.process(input);
			assertNotNull(result);
			assertEquals(AlertType.BLOOD_PRESSURE, result.getType());
			assertEquals(AlertSeverity.HIGH, result.getSeverity());
		} else {
			fail("Cannot access testcase file.");
		}
	}

	static String readFile(String path) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, Charset.defaultCharset());
		} catch (IOException ioe) {

		}
		return null;
	}

}
