package g4;

import g4.component.HeartMonitor;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class HeartMonitorDriver {

	public static void main(String[] args) {
		System.out.println("[Group 4] The Happy Heart Monitor");
		try {
			HeartMonitor heartMonitor = new HeartMonitor();
			BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));
			
			String line = null;
			while ((line = buffReader.readLine()) != null) {
				String output = (heartMonitor.process(line)).describe();
				System.out.println(output);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
