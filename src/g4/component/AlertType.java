package g4.component;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public enum AlertType {
	PULSE(3),
	OXYGEN_LEVEL(2),
	BLOOD_PRESSURE(1),
	BAD_DATA(0);
	private int typeValue = 3;

	private AlertType(int _typeValue) {
		typeValue = _typeValue;
	}

	public int getTypeValue() {
		return (typeValue);
	}

	@Override
	public String toString() {
		String type = "Unknown";
		switch (this) {
			case PULSE:
				type = "Pulse";
				break;
			case OXYGEN_LEVEL:
				type = "Oxygen level";
				break;
			case BLOOD_PRESSURE:
				type = "Blood pressure";
				break;
			case BAD_DATA:
				type = "Bad data";
				break;
		}

		return type;
	}
}
