package g4.component;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public enum AlertSeverity {
	HIGH(5),
	MEDIUM(4),
	LOW(3),
	IMPOSSIBLE(2),
	INVALID_DATA(1),
	NORMAL(0);
	private int severityValue = 0;

	private AlertSeverity(int _severityValue) {
		severityValue = _severityValue;
	}

	public int getSeverityValue() {
		return (severityValue);
	}
	
	public String getLabel() {
		String label = "";
		switch (this) {
			case HIGH:
				label = "HIGH";
				break;
			case MEDIUM:
				label = "MEDIUM";
				break;
			case LOW:
				label = "LOW";
				break;
			case IMPOSSIBLE:
				label = "LOW";
				break;
			case INVALID_DATA:
				label = "LOW";
				break;
			case NORMAL:
				label = "NORMAL";
				break;
		}

		return label;
	}

	@Override
	public String toString() {
		String severity = "";
		switch (this) {
			case HIGH:
				severity = "HIGH";
				break;
			case MEDIUM:
				severity = "MEDIUM";
				break;
			case LOW:
				severity = "LOW";
				break;
			case IMPOSSIBLE:
				severity = "IMPOSSIBLE";
				break;
			case INVALID_DATA:
				severity = "INVALID_DATA";
				break;
			case NORMAL:
				severity = "NORMAL";
				break;
		}

		return severity;
	}
}
