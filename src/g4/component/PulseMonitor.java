package g4.component;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class PulseMonitor {

	private static final int MAX_PULSE = 260;
	private static final int MIN_PULSE = 0;
	private static final int MIN_INPUT_LENGTH = 1;

	public Alert process(String _input) {
		Alert alert = null;
		_input = _input.trim();

		if (validate(_input)) {
			int pulse = getValue(_input);
			alert = new Alert(AlertType.PULSE);
			alert = decideSeverity(alert, pulse);
			alert.setValue(pulse);
		} else if (_input.length() >= MIN_INPUT_LENGTH) {
			alert = getDefaultAlert();
			alert.setValue(_input);
		}

		return (alert);
	}

	public static Alert decideSeverity(Alert _alert, int _pulse) {
		AlertSeverity severity = null;
		AlertTier tier = null;

		if ((_pulse >= 0) && (_pulse <= 19)) {
			severity = AlertSeverity.HIGH;
			tier = AlertTier.LOWER;
		} else if ((_pulse >= 20) && (_pulse <= 39)) {
			severity = AlertSeverity.MEDIUM;
			tier = AlertTier.LOWER;
		} else if ((_pulse >= 40) && (_pulse <= 110)) {
			severity = AlertSeverity.NORMAL;
			tier = AlertTier.MIDDLE;
		} else if ((_pulse >= 111) && (_pulse <= 130)) {
			severity = AlertSeverity.LOW;
			tier = AlertTier.UPPER;
		} else if ((_pulse >= 131) && (_pulse <= 170)) {
			severity = AlertSeverity.MEDIUM;
			tier = AlertTier.UPPER;
		} else if ((_pulse >= 171) && (_pulse <= 210)) {
			severity = AlertSeverity.HIGH;
			tier = AlertTier.UPPER;
		} else if ((_pulse >= 211) && (_pulse <= 260)) {
			severity = AlertSeverity.IMPOSSIBLE;
			tier = AlertTier.MIDDLE;
		}

		_alert.setSeverity(severity);
		_alert.setTier(tier);

		return (_alert);
	}

	public static boolean validate(String _input) {
		boolean validity = false;
		try {
			if (_input.length() >= MIN_INPUT_LENGTH) {
				int pulseValue = getValue(_input);
				if ((pulseValue >= MIN_PULSE) && (pulseValue <= MAX_PULSE)) {
					validity = true;
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}

		return validity;
	}

	public static int getValue(String _input) {
		return (Integer.parseInt(_input));
	}

	private Alert getDefaultAlert() {
		Alert alert = new Alert(AlertType.PULSE, AlertSeverity.INVALID_DATA, AlertTier.MIDDLE);

		return (alert);
	}
}
