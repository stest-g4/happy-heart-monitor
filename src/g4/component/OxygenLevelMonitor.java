package g4.component;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class OxygenLevelMonitor {

	private static final double MAX_VALUE = 100.0;
	private static final double MIN_VALUE = 0.0;
	private static final int STORAGE_CAPACITY = 6;
	private static final int MAX_EMPTY_CYCLE = 2;
	private static final int MIN_INPUT_LENGTH = 1;
	private Queue<Double> valueStorage = null;
	private boolean fistInsertion = true;
	private int emptyInputCounter = 0;

	public OxygenLevelMonitor() {
		valueStorage = new LinkedList<Double>();
	}

	public Alert process(String _input) {
		Alert alert = null;

		if (validate(_input)) {
			resetEmptyInputCounter();
			double oxygenLevel = getValue(_input);

			alert = new Alert(AlertType.OXYGEN_LEVEL);
			alert = getAverageAlert(alert, oxygenLevel);
			alert = decideSeverity(alert);
		} else {
			if (_input.length() >= MIN_INPUT_LENGTH) {
				alert = getDefaultAlert();
				alert.setValue(_input);
			} else if (!fistInsertion) {
				increaseEmptyInputCounter();
				alert = usePreviousValue();
			}else{
				increaseEmptyInputCounter();
			}
		}

		return (alert);
	}

	private int increaseEmptyInputCounter() {
		++emptyInputCounter;
		if (emptyInputCounter >= MAX_EMPTY_CYCLE) {
			fistInsertion = false;
		}
		return (emptyInputCounter);
	}

	private int resetEmptyInputCounter() {
		emptyInputCounter = 0;
		return (emptyInputCounter);
	}

	public static boolean validate(String _input) {
		boolean validity = false;
		try {
			_input = _input.trim();
			if (_input.length() >= 1) {
				double oxygenLevel = getValue(_input);
				if ((oxygenLevel >= MIN_VALUE) && (oxygenLevel <= MAX_VALUE)) {
					validity = true;
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}

		return validity;
	}

	public static Alert decideSeverity(Alert _alert) {
		double oxygenLevel = (Double) _alert.getValue();
		AlertSeverity severity = null;
		AlertTier tier = null;

		if ((oxygenLevel >= 0.1) && (oxygenLevel <= 49.9)) {
			severity = AlertSeverity.HIGH;
			tier = AlertTier.LOWER;
		} else if ((oxygenLevel >= 50.0) && (oxygenLevel <= 79.9)) {
			severity = AlertSeverity.MEDIUM;
			tier = AlertTier.LOWER;
		} else if ((oxygenLevel >= 80.0) && (oxygenLevel <= 84.9)) {
			severity = AlertSeverity.LOW;
			tier = AlertTier.LOWER;
		} else if ((oxygenLevel >= 85.0) && (oxygenLevel <= 99.9)) {
			severity = AlertSeverity.NORMAL;
			tier = AlertTier.MIDDLE;
		} else if ((oxygenLevel == 0.0) || (oxygenLevel == 100.0)) {
			severity = AlertSeverity.IMPOSSIBLE;
			tier = AlertTier.MIDDLE;
		}

		_alert.setSeverity(severity);
		_alert.setTier(tier);

		return (_alert);
	}

	public static double getValue(String _input) {
		double oxygenLevelValue = Double.parseDouble(_input);

		return (format(oxygenLevelValue));
	}

	public static double format(double _input) {
		DecimalFormat df = new DecimalFormat("#.#");
		return Double.valueOf(df.format(_input));
	}

	private boolean hasStoredValue() {
		return (valueStorage.size() > 0);
	}

	private Alert getAverageAlert(Alert _alert, double _oxygenLevel) {
		double average = _oxygenLevel;

		int size = valueStorage.size();
		if (size == STORAGE_CAPACITY) {
			valueStorage.poll();
		}

		valueStorage.add(_oxygenLevel);
		average = calculateAverage();

		_alert.setValue(average);
		fistInsertion = false;
		
		return (_alert);
	}

	private double calculateAverage() {
		Double average = 0.0;

		Iterator<Double> iterator = valueStorage.iterator();
		while (iterator.hasNext()) {
			average += iterator.next();
		}

		average /= valueStorage.size();

		return (format(average));
	}

	private Double getLastValue() {
		Double lastValue = null;

		if (hasStoredValue()) {
			LinkedList<Double> linkedList = (LinkedList<Double>) valueStorage;
			lastValue = linkedList.get(linkedList.size() - 1);
		}

		return (lastValue);
	}

	private Alert usePreviousValue() {
		Alert alert = new Alert(AlertType.OXYGEN_LEVEL);

		if (emptyInputCounter <= MAX_EMPTY_CYCLE) {
			Double lastValue = getLastValue();

			if (lastValue != null) {
				alert = getAverageAlert(alert, lastValue);
				alert = decideSeverity(alert);
			}
		} else {
			alert.setSeverity(AlertSeverity.INVALID_DATA);
			resetStorage();
		}

		return (alert);
	}

	private void resetStorage() {
		valueStorage.clear();
	}

	private Alert getDefaultAlert() {
		Alert alert = new Alert(AlertType.OXYGEN_LEVEL, AlertSeverity.INVALID_DATA, AlertTier.MIDDLE);

		return (alert);
	}
}
