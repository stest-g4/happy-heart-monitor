package g4.component;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class Alert implements Comparable<Alert> {

	private AlertType type = null;
	private AlertSeverity severity = null;
	private AlertTier tier = null;
	private Object value = null;
	private String timestamp = null;

	public Alert() {
		timestamp = "";
	}

	public Alert(AlertType _type) {
		timestamp = "";
		type = _type;
	}

	public Alert(AlertType _type, AlertSeverity _severity) {
		timestamp = "";
		type = _type;
		severity = _severity;
	}

	public Alert(AlertType _type, AlertSeverity _severity, AlertTier _tier) {
		timestamp = "";
		type = _type;
		severity = _severity;
		tier = _tier;
	}

	public Alert(AlertType _type, AlertSeverity _severity, AlertTier _tier, Object _value) {
		timestamp = "";
		type = _type;
		severity = _severity;
		tier = _tier;
		value = _value;
	}

	public void setTimestamp(String _timestamp) {
		timestamp = _timestamp;
	}

	public AlertType getType() {
		return (type);
	}

	public void setSeverity(AlertSeverity _severity) {
		severity = _severity;
	}

	public AlertSeverity getSeverity() {
		return (severity);
	}

	public void setTier(AlertTier _tier) {
		tier = _tier;
	}

	public AlertTier getTier() {
		return (tier);
	}

	public void setValue(Object _value) {
		value = _value;
	}

	public Object getValue() {
		return (value);
	}

	public boolean hasValue() {
		return (value != null);
	}

	public String getStrValue() {
		String strValue = null;
		if (hasValue()) {
			strValue = "(" + value.toString() + ")";
		}
		return (strValue);
	}

	public String describe() {
		String description = timestamp + " " + toString();

		return (description);
	}

	@Override
	public String toString() {
		String alert = severity.getLabel() + ": " + type.toString();

		if (severity == AlertSeverity.HIGH) {
			alert += " dangerously";
		} else if (severity == AlertSeverity.MEDIUM) {
			alert += " too";
		} else if (severity == AlertSeverity.LOW) {
			alert += "";
		} else if (severity == AlertSeverity.IMPOSSIBLE) {
			alert += " impossible. Equipment malfunction";
		} else if (severity == AlertSeverity.INVALID_DATA) {
			alert += ". Equipment malfunction";
		} else if (severity == AlertSeverity.NORMAL) {
			// atention on this one,a very special case.
			alert = "Everything is normal";
		}

		if (tier == AlertTier.LOWER) {
			alert += " low";
		} else if (tier == AlertTier.UPPER) {
			alert += " high";
		}

		if (value != null && severity != AlertSeverity.NORMAL) {
			alert += " " + getStrValue();
		}

		return alert;
	}

	@Override
	public int compareTo(Alert _otherAlert) {
		int mySeverity = severity.getSeverityValue();
		int otherSeverity = _otherAlert.severity.getSeverityValue();
		int comparison = otherSeverity - mySeverity;

		if (comparison == 0) {
			int myType = type.getTypeValue();
			int otherType = _otherAlert.type.getTypeValue();
			comparison = otherType - myType;
		}

		return comparison;
	}

}
