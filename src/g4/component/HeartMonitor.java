package g4.component;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class HeartMonitor {

	private static final String TOKEN_SEPARATOR = " ";
	private static final int TOKEN_LIMIT = 3;
	private static final int MIN_TOKEN_LENGTH = 1;

	private PulseMonitor pulseMonitor = null;
	private OxygenLevelMonitor oxygenLevelMonitor = null;
	private BloodPressureMonitor bloodPressureMonitor = null;
	private PriorityQueue<Alert> priorityQueue = null;
	private Timer timer = null;

	public HeartMonitor() {
		priorityQueue = new PriorityQueue<Alert>();
		pulseMonitor = new PulseMonitor();
		oxygenLevelMonitor = new OxygenLevelMonitor();
		bloodPressureMonitor = new BloodPressureMonitor();
		timer = new Timer();
	}

	public Alert process(String _input) {
		Alert alert = getDefaultAlert();
		String[] token = _input.split(TOKEN_SEPARATOR, TOKEN_LIMIT + 1);

		//System.out.println(Arrays.toString(token) + " | " + token.length);
		if ((token.length == TOKEN_LIMIT) && (token[0].length() >= MIN_TOKEN_LENGTH)) {
			resetQueue();
			addToQueue(pulseMonitor.process(token[0]));
			addToQueue(oxygenLevelMonitor.process(token[1]));
			addToQueue(bloodPressureMonitor.process(token[2]));
			alert = getAlert();
			alert.setTimestamp(timer.toString());

			timer.tick();
		} else {
			alert.setTimestamp(timer.toString());
		}

		return (alert);
	}

	public boolean validateFormat(String _string) {
		boolean validated = false;

		return (validated);
	}

	private void resetQueue() {
		priorityQueue.clear();
	}

	private Alert getAlert() {
		return (priorityQueue.poll());
	}

	private boolean addToQueue(Alert _alert) {
		boolean isAdded = false;

		if (_alert != null) {
			isAdded = priorityQueue.add(_alert);
		}

		return (isAdded);
	}

	private Alert getDefaultAlert() {
		Alert alert = new Alert(AlertType.BAD_DATA, AlertSeverity.INVALID_DATA, AlertTier.MIDDLE);
		return (alert);
	}
}
