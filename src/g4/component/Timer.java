package g4.component;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class Timer {

	private static final int TICK_TIME = 10;

	private static final int MAX_MM = 60;

	private static final int MAX_SS = 60;

	private int mm = 0;
	private int ss = 0;

	public void tick() {
		this.ss += TICK_TIME;
		if (this.ss == MAX_SS) {
			this.ss = 0;
			this.mm++;
		}
		if (this.mm == MAX_MM) {
			this.mm = 0;
		}
	}

	public String getTimestamp() {
		return (toString());
	}

	@Override
	public String toString() {
		String time = String.format("%02d", this.mm) + ":" + String.format("%02d", this.ss);
		return time;
	}

}
