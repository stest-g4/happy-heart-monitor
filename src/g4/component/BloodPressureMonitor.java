package g4.component;

import java.text.DecimalFormat;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public class BloodPressureMonitor {

	private static final String PRESSURE_SEPARATOR = "/";
	private static final int MAX_SYSTOLE = 260;
	private static final int MAX_DIASTOLE = 150;
	private static final int MIN_PRESSURE = 0;
	private static final int MIN_INPUT_LENGTH = 1;

	private Alert existingAlert = null;

	public Alert process(String _input) {
		Alert alert = null;

		_input = _input.trim();
		if (validate(_input)) {
			// element 0: systole, 1: diastole
			int[] pressureValue = getValue(_input);

			alert = new Alert(AlertType.BLOOD_PRESSURE);
			alert = decideSeverity(alert, pressureValue[0], pressureValue[1]);
			alert.setValue(pressureValue[0] + PRESSURE_SEPARATOR + pressureValue[1]);
			updateExistingAlert(alert);
		} else if ((_input.length() == 0) && hasExistingAlert()) {
			alert = existingAlert;
		} else if (_input.length() >= MIN_INPUT_LENGTH) {
			alert = getDefaultAlert();
			alert.setValue(_input);
		}

		return (alert);
	}

	public static boolean validate(String _input) {
		boolean validity = false;
		try {
			if (_input.length() >= MIN_INPUT_LENGTH) {
				int[] pressureValue = getValue(_input);

				if ((pressureValue.length == 2)
						&& (((pressureValue[0] >= MIN_PRESSURE)
						&& (pressureValue[0] <= MAX_SYSTOLE))
						|| ((pressureValue[1] >= MIN_PRESSURE)
						&& (pressureValue[1] <= MAX_DIASTOLE)))) {
					validity = true;
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}

		return (validity);
	}

	public static Alert decideSeverity(Alert _alert, int _systole, int _diastole) {
		AlertSeverity severity = null;
		AlertSeverity systoleSeverity = null;
		AlertSeverity diastoleSeverity = null;

		AlertTier tier = null;
		AlertTier systoleTier = null;
		AlertTier diastoleTier = null;

		if ((_systole >= 0) && (_systole <= 49)) {
			systoleSeverity = AlertSeverity.HIGH;
			systoleTier = AlertTier.LOWER;
		} else if ((_systole >= 50) && (_systole <= 69)) {
			systoleSeverity = AlertSeverity.MEDIUM;
			systoleTier = AlertTier.LOWER;
		} else if ((_systole >= 70) && (_systole <= 150)) {
			systoleSeverity = AlertSeverity.NORMAL;
			systoleTier = AlertTier.MIDDLE;
		} else if ((_systole >= 151) && (_systole <= 200)) {
			systoleSeverity = AlertSeverity.LOW;
			systoleTier = AlertTier.UPPER;
		} else if ((_systole >= 201) && (_systole <= 230)) {
			systoleSeverity = AlertSeverity.MEDIUM;
			systoleTier = AlertTier.UPPER;
		} else if ((_systole >= 231) && (_systole <= 260)) {
			systoleSeverity = AlertSeverity.IMPOSSIBLE;
			systoleTier = AlertTier.MIDDLE;
		} else {
			systoleSeverity = AlertSeverity.INVALID_DATA;
			systoleTier = AlertTier.MIDDLE;
		}

		if ((_diastole >= 0) && (_diastole <= 32)) {
			diastoleSeverity = AlertSeverity.HIGH;
			diastoleTier = AlertTier.LOWER;
		} else if ((_diastole >= 33) && (_diastole <= 39)) {
			diastoleSeverity = AlertSeverity.MEDIUM;
			diastoleTier = AlertTier.LOWER;
		} else if ((_diastole >= 40) && (_diastole <= 90)) {
			diastoleSeverity = AlertSeverity.NORMAL;
			diastoleTier = AlertTier.MIDDLE;
		} else if ((_diastole >= 91) && (_diastole <= 120)) {
			diastoleSeverity = AlertSeverity.LOW;
			diastoleTier = AlertTier.UPPER;
		} else if ((_diastole >= 121) && (_diastole <= 150)) {
			diastoleSeverity = AlertSeverity.MEDIUM;
			diastoleTier = AlertTier.UPPER;
		} else {
			diastoleSeverity = AlertSeverity.INVALID_DATA;
			diastoleTier = AlertTier.MIDDLE;
		}

		severity = systoleSeverity;
		tier = systoleTier;

		if (diastoleSeverity.getSeverityValue() > severity.getSeverityValue()) {
			severity = diastoleSeverity;
			tier = diastoleTier;
		}

		_alert.setSeverity(severity);
		_alert.setTier(tier);

		return (_alert);
	}

	public static int[] getValue(String _input) {
		String[] token = _input.split(PRESSURE_SEPARATOR);

		int[] pressureValue = new int[2];
		pressureValue[0] = Integer.parseInt(token[0]);
		pressureValue[1] = Integer.parseInt(token[1]);

		return (pressureValue);
	}

	private boolean hasExistingAlert() {
		return (existingAlert != null);
	}

	private Alert updateExistingAlert(Alert _newAlert) {
		Alert oldAlert = existingAlert;
		existingAlert = _newAlert;

		return (oldAlert);
	}

	private Alert getDefaultAlert() {
		Alert alert = new Alert(AlertType.BLOOD_PRESSURE, AlertSeverity.INVALID_DATA, AlertTier.MIDDLE);

		return (alert);
	}

}
