package g4.component;

/**
 * @author Group 4
 * @since 1.0
 * @version 1.0
 */
public enum AlertTier {
	UPPER(1),
	MIDDLE(0),
	LOWER(-1);
	private int tierValue = 0;

	private AlertTier(int _tierValue) {
		tierValue = _tierValue;
	}

	@Override
	public String toString() {
		String tier = "";
		switch (this) {
			case UPPER:
				tier = "high";
				break;
			case MIDDLE:
				tier = "";
				break;
			case LOWER:
				tier = "low";
				break;
		}

		return tier;
	}
}
