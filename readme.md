## Synopsis
This repository is dedicated for Software Testing course project at VU Amsterdam.


## The Happy Heart Monitoring
The program monitors the heart functions of a hospital patient. Its main purpose is to raise alarms if there is something seriously wrong. Specifically, it monitors the following:
* Pulse rate
* Blood pressure
* Blood oxygen level

If this were a real system, it would be software in a special machine with sensors providing the data. But since we don’t have any fancy hardware (or any real patients, for that matter), we will make do with simulated data.

## The Happy Heart Monitoring
To execute, run from the command line:
```
java -jar The_Happy_Heart_Monitor.jar < [input_file_path]
```
for example (executing under the "dist" directory):
```
java -jar The_Happy_Heart_Monitor.jar < ..\test-input\TC-151.txt
```

## Contributors
Group 4